<?php

namespace App\Controller;

use App\Entity\Work;
use App\Form\Type\WorkType;
use App\Form\WorkFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class WorkController extends AbstractController
{
    /**
     * @Route("/work/", name="_FormFlow_Work")
     */
    public function Work(WorkFlow $flow) {
      return $this->processFlow(new Work(), $flow,
          'form/form.html.twig');
    }

    protected function processFlow($formData, FormFlowInterface $flow, $template) {
      $flow->bind($formData);

      $form = $submittedForm = $flow->createForm();
      if ($flow->isValid($submittedForm)) {
        $flow->saveCurrentStepData($submittedForm);

        if ($flow->nextStep()) {
          // create form for next step
          $form = $flow->createForm();
        } else {
          // flow finished
          $em = $this->getDoctrine()->getManager();
          $em->persist($formData);
          $em->flush();

          $flow->reset();

          return $this->redirect($this->generateUrl('_FormFlow_Work'));
        }
      }

      if ($flow->redirectAfterSubmit($submittedForm)) {
        $request = $this->get('request_stack')->getCurrentRequest();
        $params = $this->get('craue_formflow_util')->addRouteParameters(array_merge($request->query->all(),
            $request->attributes->get('_route_params')), $flow);

        return $this->redirect($this->generateUrl($request->attributes->get('_route'), $params));
      }

      return $this->render($template, [
        'form' => $form->createView(),
        'flow' => $flow,
        'formData' => $formData,
      ]);
    }
  }
