<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function homepage(): Response
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/il-progetto", name="project")
     */
    public function project(): Response
    {
        return $this->render('default/project.html.twig');
    }

    /**
     * @Route("/privacy-policy", name="privacy_policy")
     */
    public function privacyPolicy(): Response
    {
        return $this->render('default/privacy_policy.html.twig');
    }

    /**
     * @Route("/cookie-policy", name="cookie_policy")
     */
    public function cookiePolicy(): Response
    {
        return $this->render('default/cookie_policy.html.twig');
    }
}
