<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\Type\UserType;
use App\Form\UserFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class UserController extends Controller
{
	  /**
		 * @Route("/user/", name="_FormFlow_User")
		 */
		public function User(UserFlow $flow) {
			return $this->processFlow(new User(), $flow,
					'form/form.html.twig');
		}

    /**
     * @Route("/osas/test/", name="osas_test")
     */
    public function home(): Response
    {
        return $this->render('form/index.html.twig', [
            'controller_name' => 'UserController'
        ]);
    }

    protected function processFlow($formData, FormFlowInterface $flow, $template) {
  		$flow->bind($formData);

  		$form = $submittedForm = $flow->createForm();
  		if ($flow->isValid($submittedForm)) {
  			$flow->saveCurrentStepData($submittedForm);

  			if ($flow->nextStep()) {
  				// create form for next step
  				$form = $flow->createForm();
  			} else {
  				// flow finished
					$em = $this->getDoctrine()->getManager();
					$em->persist($formData);
					$em->flush();

  				$flow->reset();

  				return $this->redirect($this->generateUrl('_FormFlow_Work'));
  			}
  		}

  		if ($flow->redirectAfterSubmit($submittedForm)) {
  			$request = $this->get('request_stack')->getCurrentRequest();
  			$params = $this->get('craue_formflow_util')->addRouteParameters(array_merge($request->query->all(),
  					$request->attributes->get('_route_params')), $flow);

  			return $this->redirect($this->generateUrl($request->attributes->get('_route'), $params));
  		}

  		return $this->render($template, [
  			'form' => $form->createView(),
  			'flow' => $flow,
  			'formData' => $formData,
  		]);
  	}

}
