<?php

namespace App\Entity;

use App\Repository\WorkRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WorkRepository::class)
 */
class Work
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $employed;

    /**
     * @ORM\Column(type="date")
     */
    private $since;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmployed(): ?string
    {
        return $this->employed;
    }

    public function setEmployed(string $employed): self
    {
        $this->employed = $employed;

        return $this;
    }

    public function getSince(): ?\DateTimeInterface
    {
        return $this->since;
    }

    public function setSince(\DateTimeInterface $since): self
    {
        $this->since = $since;

        return $this;
    }
}
