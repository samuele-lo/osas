<?php

namespace App\Form;

use App\Form\Type\WorkType;
use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WorkFlow extends FormFlow {

  /**
   * {@inheritDoc}
   */
  protected $formType;
  //
  // public function setFormType(FormTypeInterface $formType) {
  //   $this->formType = $formType;
  // }
  //
  // public function getName() {
  //   return 'User';
  // }


	protected function loadStepsConfig() {

    $formType = WorkType::class;

		return [
			[
				'label' => 'Genere',
        'form_type' => WorkType::class,
				// 'form_type' => UserType::class,
        // 'skip' => true,
			],
			[
				'label' => 'Titolo di Studio',
				'form_type' => WorkType::class,
				// 'skip' => function($estimatedCurrentStepNumber, FormFlowInterface $flow) {
				// 	return $estimatedCurrentStepNumber > 1 && !$flow->getFormData()->canHaveEngine();
				// },
			],
			[
				'label' => 'Conferma',
			],
		];
	}

}
