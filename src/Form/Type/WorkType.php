<?php

namespace App\Form\Type;

use App\Entity\Work;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class WorkType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      switch ($options['flow_step']) {
        case 1:
          $validValues = ['Si', 'No'];
          $builder
          ->add('employed', TextType::class, [
            // 'choices' => array_combine($validValues, $validValues),
            // 'choices' => [
            //   'si' => 'si',
            //   'no' => 'no'
            // ],
            'label' => 'Lavori',
          ]);
          // ->add('since', BirthdayType::class, [
          //   'label' => 'Data di nascita',
          // ]);
          break;
        case 2:
          // $validValues = ['Licenza Scuola Elementare','Scuola Media','Scuola Superiore','Laurea', 'Non in elenco'];
          $builder
          ->add('since', BirthdayType::class, [
            'label' => 'Data di nascita',
          ]);
          // ->add('degree', ChoiceType::class, [
          //   'label' => 'Titolo di Studio',
          //   'choices' => array_combine($validValues, $validValues),
          // ]);
          // ->add('degree', ChoiceType::class, [
          //   'label' => 'Non in elenco? Inseriscilo tu',
          //   'placeholder' => 'Inserisci il titolo',
          //   'choices' => array_combine($validValues, $validValues),
          // ]);
          break;
      }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Work::class,
        ]);
    }
}
